unit ObjPolinomio;

{$mode objfpc}{$H+}

interface

uses  // Dialogs
  Classes, SysUtils, TerminoPolinomio;

const
  MIN = 1;
  MAX = 10;

type
  // Objeto que representa una Lista de Terminos tipo TerminoP (Un polinomio)
  Polinomio = Object
    private
      items : Array[MIN..MAX] of TerminoP;
      cantidad : integer;
      function posicionValida(i : integer) : boolean;
    public
      procedure initPolinomio();
      function esVacia() : boolean;
      function esLlena() : boolean;
      function getCantidad() : integer;
      function agregarTermino(  t : TerminoP; var msj : String) : boolean;
      function insertarTermino( t : TerminoP ; x : integer; var msj : String) : boolean;
      function modificarTermino(t : TerminoP ; x : integer; var msj : String) : boolean;
      function eliminarTermino(x : integer; var msj : String) : boolean;
      function listaToString() : String;
      function valorPolinomio(x : real) : real;
      function getPolinomio(var t : TerminoP ; x : integer; var msj : String) : boolean;
      function exponenteExistente(exp : integer) : boolean;
      function TconstanteExistente() : boolean;
  end;

implementation

procedure Polinomio.initPolinomio();
begin
   cantidad := 0;
end;


function Polinomio.esVacia() : boolean;
begin
   esVacia := (cantidad = 0);
end;


function Polinomio.esLlena() : boolean;
begin
   esLlena := (cantidad = MAX);
end;


(* Funcion que retorna la cantidad de terminos actuales en la lista de Polinomios *)
function Polinomio.getCantidad() : integer;
begin
   getCantidad := cantidad;
end;


function Polinomio.posicionValida(i : integer) : boolean;
begin
  if ( i >= MIN ) and ( i <= getCantidad() ) then
    posicionValida := true
  else
    posicionValida := false;
end;


(* Funcion que recibe un TerminoP por referencia y una posicion 'x', si la posicion
 es valida setea el terminoP recibido con la informacion del termino DE la lista. *)
function Polinomio.getPolinomio(var t : TerminoP ; x : integer; var msj : String) : boolean;
var
  b : String;
begin
  getPolinomio := false;
  if posicionValida(x) then begin
    t.setCoeficiente( items[x].getCoeficiente(), b);
    t.setExponente( items[x].getExponente(), b);
    t.setEsConstante( items[x].getEsConstante() );
    getPolinomio := true;
  end else
    msj := 'Posicion Invalida!';
end;


(* Funcion que agrega un Termino al polinomio *)
function Polinomio.agregarTermino(t : TerminoP ; var msj : String) : boolean;
begin
  agregarTermino := false;
  if esLlena() then begin
    msj := 'Polinomio Completo!';
    exit;
  end;
  if t.getEsConstante() then begin // si es un termino constante
    if TconstanteExistente() then begin // si ya existe un termino constante en el polinomio
      msj := 'Ya existe un termino constante en el polinomio';
      exit;
    end;
  end else begin // si no es termino constante
    if exponenteExistente(t.getExponente()) then begin // si ya existe un termino con este exponente
      msj := 'Ya existe un termino con Exponente '+inttostr(t.getExponente())+' en el polinomio';
      exit;
    end;
  end;
  // sin problemas, entonces agrego ...
  inc(cantidad);
  items[cantidad] := t;
  agregarTermino := true;
end;


(* Funcion que inserta un Termino al polinomio, se valida previamente *)
function Polinomio.insertarTermino(t : TerminoP ; x : integer; var msj : String) : boolean;
var
  i : integer;
begin
  insertarTermino := false;
  if not posicionValida(x) then begin
    msj := 'Posicion Invalida!';
    exit;
  end;
  if esVacia() then begin
    msj := 'Polinomio Vacio!';
    exit;
  end;
  if esLlena() then begin
    msj := 'Polinomio Completo!';
    exit;
  end;
  if t.getEsConstante() then begin // si es un termino constante
    if TconstanteExistente() then begin
      msj := 'Ya existe un termino constante en el polinomio';
      exit;
    end;
  end else begin // si no es termino constante
    if exponenteExistente(t.getExponente()) then begin
      msj := 'Ya existe un termino con Exponente '+inttostr(t.getExponente())+' en el polinomio';
      exit;
    end;
  end;
  // sin problemas, entonces inserto ...
  for i := getCantidad() downto x do
    items[i+1] := items[i];
  items[x] := t;
  inc(cantidad);
  insertarTermino := true;
end;


(* Funcion que elimina un termino del polinomio segun sea la posicion 'x' *)
function Polinomio.eliminarTermino(x : integer; var msj : String) : boolean;
var
  i : integer;
begin
  eliminarTermino := false;
  if esVacia() then begin
    msj := 'Polinomio Vacio!';
    exit;
  end;
  if not posicionValida(x) then begin
    msj := 'Posicion Incorrecta!';
    exit;
  end;
  for i := x to getCantidad()-1 do
    items[i] := items[i+1];
  Dec(cantidad);
  eliminarTermino := true;
end;


(* Funcion que modifica un termino del polinomio *)
function Polinomio.modificarTermino(t : TerminoP ; x : integer; var msj : String) : boolean;
begin
  modificarTermino := false;
  if not posicionValida(x) then begin
    msj := 'Posicion Invalida!';
    exit;
  end;
  if exponenteExistente(t.getExponente()) and
     ( items[x].getExponente() <> t.getExponente() ) then begin
    msj := 'Ya existe un termino con Exponente '+inttostr(t.getExponente())+' en el polinomio';
    exit;
  end;
  items[x] := t;
  modificarTermino := true;
end;


(* Funcion que retorna una reprensentacion del Polinomio en formato String *)
function Polinomio.listaToString() : String;
var
  i : integer;
  buffer : String;
begin
  buffer := '';
  for i := MIN to getCantidad() do begin
    if (i > 1) and (items[i].getCoeficiente() > 0) then
      // por una cuestion de estetica, se le agrega el '+' a los terminos siguientes
      //al primero que sean positivos
      buffer := buffer + '+' + items[i].terminoToString()
    else
      buffer := buffer + items[i].terminoToString();
  end;
  listaToString := buffer;
end;


(* Funcion que retorna True si un exponente 'exp' ya existe en el polinomio, False
   en caso contrario. *)
function Polinomio.exponenteExistente(exp : integer) : boolean;
var
  i : integer;
  existe : boolean;
begin
  i := MIN;
  existe := false;
  while (i <= getCantidad()) and not existe do begin
    if items[i].getExponente() = exp  then
      existe := true;
    inc(i);
  end;
  exponenteExistente := existe;
end;


(* Funcion que retorna True si un termino constante ya existe en el polinomio, False
   en caso contrario. *)
function Polinomio.TconstanteExistente() : boolean;
var
  i : integer;
  existe : boolean;
begin
  i := MIN;
  existe := false;
  while (i <= getCantidad()) and not existe do begin
    if items[i].getEsConstante() = true then
      existe := true;
    inc(i);
  end;
  TconstanteExistente := existe;
end;

(* Funcion que retorna el valor del Polinomio segun sea 'x' ( F(x) ) *)
function Polinomio.valorPolinomio(x : real) : real;
var
  i : integer;
  acc : real;
begin
  acc := 0.0;
  for i := MIN to getCantidad() do begin
    acc := acc + items[i].calcularValorTerminoX(x);
  end;
  valorPolinomio := acc;
end;

end.
