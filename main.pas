unit main;

{$mode objfpc}{$H+}

interface

uses // TAChartExtentLink
  Classes, SysUtils, FileUtil, TAGraph, TASeries, TAFuncSeries,
   TASources, TAStyles, Forms, Controls, Graphics, Dialogs,
  StdCtrls, PairSplitter, ComCtrls, Menus, ExtCtrls, TerminoPolinomio,
  ObjPolinomio;

type

  { TForm1 }

  TForm1 = class(TForm)
    btnAceptarAlta: TButton;
    btnResetAll: TButton;
    btnGraficar: TButton;
    btnCalcular: TButton;
    btnBuscar: TButton;
    btnEliminar: TButton;
    btnModificar: TButton;
    ButtonClearDebug: TButton;
    CheckBoxTConstEdit: TCheckBox;
    graficoPolinomio: TChart;
    CheckBoxTConst: TCheckBox;
    ComboBoxDesde: TComboBox;
    ComboBoxHasta: TComboBox;
    ComboBoxInterv: TComboBox;
    EditExpAlta: TEdit;
    EditInsertPos: TEdit;
    EditCoefAlta: TEdit;
    EditPolComplet: TEdit;
    EditFdeX: TEdit;
    EditFdeXresult: TEdit;
    EditCantTerm: TEdit;
    EditNterm: TEdit;
    EditCoefEdit: TEdit;
    EditExpEdit: TEdit;
    graficoPolinomioLineSeries1: TLineSeries;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label2: TLabel;
    LabelPosInsert: TLabel;
    LabelXi: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Memo1: TMemo;
    RadioBtnAlta: TRadioButton;
    RadioBtnInsert: TRadioButton;
    procedure btnAceptarAltaClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnCalcularClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnGraficarClick(Sender: TObject);
    procedure btnModificarClick(Sender: TObject);
    procedure btnResetAllClick(Sender: TObject);
    procedure ButtonClearDebugClick(Sender: TObject);
    procedure CheckBoxTConstChange(Sender: TObject);
    procedure EditNtermChange(Sender: TObject);
    procedure RadioBtnAltaChange(Sender: TObject);
    procedure RadioBtnInsertChange(Sender: TObject);
    function validarEntradaInt(ted : TEdit; var msj : String) : boolean;
    function validarEntradaReal(ted : TEdit; var msj : String) : boolean;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  Pl : Polinomio;

implementation


{$R *.lfm}

{ TForm1 }


(* Procedimiento encargado de agregar o insertar un Termino al Polinomio *)
procedure TForm1.btnAceptarAltaClick(Sender: TObject);
var
  T : TerminoP;
  mensajeError : String;
begin
  mensajeError := '';
  // validacion de coeficiente y exponente
  if not validarEntradaReal(EditCoefAlta, mensajeError) or
     not validarEntradaInt(EditExpAlta, mensajeError) then begin
    ShowMessage(mensajeError);
    exit;
  end;
  // chequeando valores permitidos de exponente y coeficiente
  if not T.setExponente(strtoint(EditExpAlta.text), mensajeError) then begin
      ShowMessage(mensajeError);
      exit;
  end;
  if not T.setCoeficiente(strtofloat(EditCoefAlta.text), mensajeError) then begin
      ShowMessage(mensajeError);
      exit;
  end;
  // sin errores entrada de datos
  if CheckBoxTConst.Checked then T.setEsConstante( true ) // si elijio numero constante
  else T.setEsConstante( false );

  if RadioBtnAlta.Checked then begin // si es agregar
    if not Pl.agregarTermino(T, mensajeError) then begin
      ShowMessage(mensajeError); // error al agregar -> polinomio completo, exponente o num. constante existente.
      exit;
    end;
  end;

  if RadioBtnInsert.Checked then begin // si es insertar
    if not validarEntradaInt(EditInsertPos, mensajeError) then begin
      ShowMessage(mensajeError);
      exit;
    end;
    if not Pl.insertarTermino(T, strtoint(EditInsertPos.text), mensajeError) then begin
      ShowMessage(mensajeError);  // error al insertar -> posicion invalida, polinomio vacio o completo
      exit;
    end;
  end;

  EditCoefAlta.clear;
  EditExpAlta.clear;
  EditInsertPos.clear;
  CheckBoxTConst.Checked := false;
  EditPolComplet.text := Pl.listaToString();
  EditCantTerm.text := inttostr( Pl.getCantidad() );
end;


(* Procedimiento evento click boton "Eliminar". Elimina un termino del polinomio
   segun la posicion indicada. *)
procedure TForm1.btnEliminarClick(Sender: TObject);
var
    mensajeError : String;
begin
  mensajeError := '';
  // validacion de numero de termino a eliminar
  if not validarEntradaInt(EditNterm, mensajeError) then begin
    ShowMessage(mensajeError);
    exit;
  end;
  if not Pl.eliminarTermino( strtoint(EditNterm.text),mensajeError ) then begin
    ShowMessage(mensajeError); // error al eliminar -> posicion incorrecta, polinomio vacio
    exit;
  end;
  EditNterm.clear;
  editExpEdit.clear;
  editCoefEdit.clear;
  editExpEdit.Enabled := true;
  EditPolComplet.text := Pl.listaToString();
  EditCantTerm.text := inttostr( Pl.getCantidad() );
  ShowMessage('Termino Eliminado');
end;


(* Procedimiento evento click boton "Modificar". Modifica un termino especifico
 del Polinomio segun los valores indicados. *)
procedure TForm1.btnModificarClick(Sender: TObject);
var
  T : TerminoP;
  mensajeError : String;
begin
  mensajeError := '';
  // validacion de numero de termino a eliminar, coeficiente y exponente
  if not validarEntradaReal(editCoefEdit,mensajeError) or
     not validarEntradaInt(editExpEdit, mensajeError) or
     not validarEntradaInt(EditNterm, mensajeError) then begin
    ShowMessage(mensajeError);
    exit;
  end;

  // chequeando valores permitidos de exponente y coeficiente
  if not T.setExponente(strtoint(editExpEdit.text), mensajeError) then begin
      ShowMessage(mensajeError);
      exit;
  end;
  if not T.setCoeficiente(strtofloat(editCoefEdit.text), mensajeError) then begin
      ShowMessage(mensajeError);
      exit;
  end;
  // sin errores entrada de datos...
  if CheckBoxTConstEdit.Checked then T.setEsConstante(true)
  else T.setEsConstante(false);

 if not Pl.modificarTermino(T, strtoint(EditNterm.text), mensajeError ) then begin
    ShowMessage(mensajeError); // error al modificar -> posicion invalida, exponente ya existente
    exit;
  end;
  EditNterm.clear;
  editExpEdit.clear;
  editCoefEdit.clear;
  editExpEdit.Enabled := true;
  EditPolComplet.text := Pl.listaToString();
  EditCantTerm.text := inttostr( Pl.getCantidad() );
  ShowMessage('Termino Modificado');
end;


(* Procedimiento evento click boton "Buscar". Busca un termino en el polinomio
  segun la posicion indicada. *)
procedure TForm1.btnBuscarClick(Sender: TObject);
var
  T : TerminoP;
  mensajeError : String;
begin
  mensajeError := '';
  editCoefEdit.clear;
  editExpEdit.clear;
  editExpEdit.Enabled := true;
  CheckBoxTConstEdit.Checked := false;
  // validacion de numero de termino a buscar
  if not validarEntradaInt(EditNterm, mensajeError) then begin
    ShowMessage(mensajeError);
    exit;
  end;

  if not Pl.getPolinomio(T, strtoint(EditNterm.text), mensajeError) then begin
    ShowMessage(mensajeError); // error posicion invalida
    exit;
  end;
  if T.getEsConstante() then begin
    editExpEdit.Enabled := false;
    CheckBoxTConstEdit.Checked := true;
  end;
  editCoefEdit.text := floattostr(T.getCoeficiente());
  editExpEdit.text  := inttostr(T.getExponente());
  btnEliminar.Enabled := true;
  btnModificar.Enabled := true;
end;


(* Procedimiento evento click en boton "Calcular". Realiza el calculo del
  valor del Polinomio en funcion del valor de 'x'. *)
procedure TForm1.btnCalcularClick(Sender: TObject);
var
  x: real;
  mensajeError : String;
begin
  mensajeError := '';
  if not validarEntradaReal(EditFdeX, mensajeError) then begin
    ShowMessage(mensajeError);
    exit;
  end;
  x := strtofloat( EditFdeX.text );
  EditFdeXresult.text := floattostr( Pl.valorPolinomio( x ) );
end;





(*############################# GRAFICANDO ###################################*)
(*############################################################################*)
(* Procedimiento evento click en boton "Graficar" *)
procedure TForm1.btnGraficarClick(Sender: TObject);
var
  desde, hasta, interv: real;
begin
  graficoPolinomioLineSeries1.Clear;
  memo1.clear;
  desde  := strtofloat(ComboBoxDesde.Text);
  hasta  := strtofloat(ComboBoxHasta.text);
  interv := strtofloat( ComboBoxInterv.text);
  while desde <= hasta do begin
    graficoPolinomioLineSeries1.AddXY( desde, Pl.valorPolinomio(desde) );
    memo1.lines.add( 'x : '+ floattostr(desde) +'  y : ' + floattostr(Pl.valorPolinomio(desde) ) );
    desde := desde + interv;
  end;
end;
(*############################################################################*)
(*############################################################################*)




(* Procedimiento evento click boton "Resetear Todo" *)
procedure TForm1.btnResetAllClick(Sender: TObject);
begin
  Pl.initPolinomio();
  EditPolComplet.clear;
  EditFdeX.clear;
  EditFdeXresult.clear;
  EditCantTerm.text := '0';
  EditNterm.clear;
  EditCoefEdit.clear;
  EditExpEdit.clear;
  editExpEdit.Enabled := true;
  Memo1.clear;
  graficoPolinomioLineSeries1.Clear;
end;

procedure TForm1.ButtonClearDebugClick(Sender: TObject);
begin
  Memo1.clear;
end;


(* Procedimiento evento click en checkbox "Termino Constante " que setea algunos widgets *)
procedure TForm1.CheckBoxTConstChange(Sender: TObject);
begin
  if CheckBoxTConst.Checked then begin
    EditExpAlta.text := '1';
    EditExpAlta.enabled := false;
    EditCoefAlta.clear;
    LabelXi.enabled := false;
  end else begin
    EditExpAlta.enabled := true;
    EditCoefAlta.clear;
    EditExpAlta.clear;
    LabelXi.enabled := true;
  end;
end;

procedure TForm1.EditNtermChange(Sender: TObject);
begin
  editExpEdit.clear;
  editCoefEdit.clear;
  CheckBoxTConstEdit.Checked := false;
  editExpEdit.Enabled := true;
  btnEliminar.Enabled := false;
  btnModificar.Enabled := false;
end;


(* Procedimiento de evento click en radiobutton Agregar un termino *)
procedure TForm1.RadioBtnAltaChange(Sender: TObject);
begin
  if RadioBtnAlta.Checked then begin
    LabelPosInsert.Enabled := false;
    EditInsertPos.Enabled := false;
  end;
end;


(* Procedimiento de evento click en radiobutton Insertar un termino *)
procedure TForm1.RadioBtnInsertChange(Sender: TObject);
begin
  if RadioBtnInsert.Checked then begin
    LabelPosInsert.Enabled := true;
    EditInsertPos.Enabled := true;
  end;
end;



(* ------------------------- Validaciones -----------------------------------*)
function TForm1.validarEntradaInt( ted : TEdit; var msj : String) : boolean;
var
  iValue, iCode: Integer;
begin
  val(ted.text, iValue, iCode);
  if iCode = 0 then begin
    validarEntradaInt := true;
  end
  else begin
    msj := ted.text + ' NO es un numero Entero!';
    validarEntradaInt := false;
  end;
end;

function TForm1.validarEntradaReal( ted : TEdit; var msj : String) : boolean;
var
  iCode: Integer;
  iValue : real;
begin
  val(ted.text, iValue, iCode);
  if iCode = 0 then validarEntradaReal := true
  else begin
    msj := ted.text + ' NO es un numero Decimal!';
    validarEntradaReal := false;
  end;
end;
(*  procedure Val(S; var V; var Code: Integer);
In Delphi code, Val converts the string value S to its numeric representation,
as if it were read from a text file with Read.

S is a string-type expression; it must be a sequence of characters that form a signed real number.

V is an integer-type or real-type variable. If V is an integer-type variable,
S must form a whole number.

Code is a variable of type Integer.

If the string is invalid, the index of the offending character is stored in Code;
otherwise, Code is set to zero. For a null-terminated string, the error position returned in
Code is one larger than the actual zero-based index of the character in error. *)


end.

