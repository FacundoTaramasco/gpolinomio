unit TerminoPolinomio;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math; // Math para usar funcion Power

type
  // Objeto que representa un termino de un polinomio.( ej. 3x^2 )
  TerminoP = Object
    private
      coeficiente : real;
      exponente   : integer;
      esConstante : boolean;
      function utilidadFormateoExp()  : String;
      function utilidadFormateoCoef() : String;
    public
      // Getters
      function getCoeficiente() : real;
      function getExponente()   : integer;
      function getEsConstante() : boolean;
      // Setters
      function setCoeficiente(c : real ; var msj : String)    : boolean;
      function setExponente(e : integer; var msj : String)    : boolean;
      function setEsConstante(ec : boolean) : boolean;
      // Customs
      function calcularValorTerminoX( x : real ) : real;
      function terminoToString() : String;
  end;


implementation

// -------------------------- Getters ---------------------------------------- //

function TerminoP.getCoeficiente() : real;
begin
  getCoeficiente := coeficiente;
end;


function TerminoP.getExponente() : integer;
begin
  getExponente := exponente;
end;


function TerminoP.getEsConstante() : boolean;
begin
 getEsConstante := esConstante;
end;

// --------------------------------------------------------------------------- //


// -------------------------- Setters ---------------------------------------- //

function TerminoP.setCoeficiente(c : real; var msj : String) : boolean;
begin
  setCoeficiente := false;
  if c <> 0.0 then begin
    coeficiente := c;
    setCoeficiente := true;
  end else
    msj := 'Error, Coeficiente no puede ser 0.0';
end;


function TerminoP.setExponente(; var msj : Stringe : integer; var msj : String) : boolean;
begin
  setExponente := false;
  if (e > 0) and  (e <= 10) then begin
    exponente := e;
    setExponente := true;
  end else
    msj := 'Error, Exponente debe ser > 0 y <= 10';
end;


function TerminoP.setEsConstante( ec : boolean ) : boolean;
begin
  esConstante := ec;
  setEsconstante := true;
end;

// --------------------------------------------------------------------------- //

// -------------------------- Customs ---------------------------------------- //


(* Funcion que retorna el valor del termino segun sea F(x).
    Ej.
     F(x) = 3 x^2   (exponente == 2, coeficiente == 3, esConstante == false)
     F(1) = 3 * 1^2 --> 3 *)
function TerminoP.calcularValorTerminoX( x : real ) : real;
begin
  if getEsConstante() then
    calcularValorTerminoX := getCoeficiente()
  else
    calcularValorTerminoX := getCoeficiente() * Power(x, getExponente() );
end;


(* Funcion que retorna el valor del termino representado como un String.
   Ej.
    si el termino no es esConstante
      exponente   == 3
      coeficiente == -4
      esConstante == false
      String a retornar : -4x^3
    si el termino es una constante
     exponente == 1 ( siempre se toma como exp == 1 si es una constante)
     coeficiente == -5
     esConstante == true
     String a retornar : -5  *)
function TerminoP.terminoToString() : String;
var
  buffer : String;
begin
  buffer := '';
  if not getEsConstante() then // si no es termino constante
    buffer := utilidadFormateoCoef() + utilidadFormateoExp()
  else
    buffer := floattostr( getCoeficiente );
  terminoToString := buffer;
end;




// --------------------------------------------------------------------------- //

function TerminoP.utilidadFormateoExp() : String;
var
  buf : string;
begin
  buf := '';
  if getExponente() = 1 then buf := ''
  else buf := '^' + inttostr( getExponente() );
  utilidadFormateoExp := buf;
end;

function TerminoP.utilidadFormateoCoef() : String;
var
  buf : string;
begin
  buf := '';
  if getCoeficiente() = 1.0 then buf := 'x'
  else buf := floattostr( getCoeficiente() ) + 'x';
  utilidadFormateoCoef := buf;
end;

end.
